module gitee.com/raymond/sls

go 1.15

require (
	github.com/cenkalti/backoff v1.0.0
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-kit/kit v0.8.1-0.20190225011659-a8cc1630e08a // indirect
	github.com/go-logfmt/logfmt v0.4.0 // indirect
	github.com/gogo/protobuf v0.0.0-20171213104750-35b81a066e52
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b
	github.com/golang/protobuf v0.0.0-20170920220647-130e6b02ab05
	github.com/pierrec/lz4 v2.0.2+incompatible
	github.com/pierrec/xxHash v0.0.0-20170714082455-a0006b13c722 // indirect
	github.com/pkg/errors v0.8.0
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.1.5-0.20171018052257-2aa2c176b9da
	golang.org/x/net v0.0.0-20160826235738-6250b4127982
)
